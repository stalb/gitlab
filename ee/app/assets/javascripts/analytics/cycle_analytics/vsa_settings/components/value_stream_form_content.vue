<script>
import { GlAlert, GlButton, GlForm, GlFormInput, GlFormGroup, GlFormRadioGroup } from '@gitlab/ui';
import { cloneDeep, uniqueId } from 'lodash';
// eslint-disable-next-line no-restricted-imports
import { mapState, mapActions } from 'vuex';
import { filterStagesByHiddenStatus } from '~/analytics/cycle_analytics/utils';
import { swapArrayItems } from '~/lib/utils/array_utility';
import { sprintf } from '~/locale';
import Tracking from '~/tracking';
import { visitUrlWithAlerts, mergeUrlParams } from '~/lib/utils/url_utility';
import { getLabelEventsIdentifiers } from 'ee/analytics/cycle_analytics/utils';
import {
  validateValueStreamName,
  cleanStageName,
  validateStage,
  formatStageDataForSubmission,
  hasDirtyStage,
} from '../utils';
import {
  STAGE_SORT_DIRECTION,
  i18n,
  defaultCustomStageFields,
  PRESET_OPTIONS,
  PRESET_OPTIONS_DEFAULT,
  VSA_SETTINGS_FORM_SUBMISSION_SUCCESS_ALERT_ID,
} from '../constants';
import ValueStreamFormContentActions from './value_stream_form_content_actions.vue';
import CustomStageFields from './custom_stage_fields.vue';
import DefaultStageFields from './default_stage_fields.vue';

const initializeStageErrors = (defaultStageConfig, selectedPreset = PRESET_OPTIONS_DEFAULT) =>
  selectedPreset === PRESET_OPTIONS_DEFAULT ? defaultStageConfig.map(() => ({})) : [{}];

const initializeStages = (defaultStageConfig, selectedPreset = PRESET_OPTIONS_DEFAULT) => {
  const stages =
    selectedPreset === PRESET_OPTIONS_DEFAULT
      ? defaultStageConfig
      : [{ ...defaultCustomStageFields }];
  return stages.map((stage) => ({ ...stage, transitionKey: uniqueId('stage-') }));
};

const initializeEditingStages = (stages = []) =>
  filterStagesByHiddenStatus(cloneDeep(stages), false).map((stage) => ({
    ...stage,
    transitionKey: uniqueId(`stage-${stage.name}-`),
  }));

export default {
  name: 'ValueStreamFormContent',
  components: {
    GlAlert,
    GlButton,
    GlForm,
    GlFormInput,
    GlFormGroup,
    GlFormRadioGroup,
    DefaultStageFields,
    CustomStageFields,
    ValueStreamFormContentActions,
  },
  mixins: [Tracking.mixin()],
  inject: ['vsaPath'],
  props: {
    initialData: {
      type: Object,
      required: false,
      default: () => ({}),
    },
    defaultStageConfig: {
      type: Array,
      required: true,
    },
    isEditing: {
      type: Boolean,
      required: false,
      default: false,
    },
  },
  data() {
    const {
      defaultStageConfig = [],
      initialData: { name: initialName, stages: initialStages = [] },
    } = this;

    return {
      hiddenStages: filterStagesByHiddenStatus(initialStages),
      selectedPreset: PRESET_OPTIONS_DEFAULT,
      presetOptions: PRESET_OPTIONS,
      name: initialName,
      nameErrors: [],
      stageErrors: [{}],
      showSubmitError: false,
      isSubmitting: false,
      stages: this.isEditing
        ? initializeEditingStages(initialStages)
        : initializeStages(defaultStageConfig),
    };
  },
  computed: {
    ...mapState([
      'isFetchingGroupLabels',
      'formEvents',
      'defaultGroupLabels',
      'createValueStreamErrors',
      'selectedValueStream',
    ]),
    selectedValueStreamId() {
      return this.selectedValueStream?.id || -1;
    },
    isValueStreamNameValid() {
      return !this.nameErrors?.length;
    },
    invalidNameFeedback() {
      return this.nameErrors?.length ? this.nameErrors.join('\n\n') : null;
    },
    hasFormErrors() {
      return Boolean(
        this.nameErrors.length || this.stageErrors.some((obj) => Object.keys(obj).length),
      );
    },
    isDirtyEditing() {
      return (
        this.isEditing &&
        (this.hasDirtyName(this.name, this.initialData.name) ||
          hasDirtyStage(this.stages, this.initialData.stages))
      );
    },
    canRestore() {
      return this.hiddenStages.length || this.isDirtyEditing;
    },
    currentValueStreamStageNames() {
      return this.stages.map(({ name }) => cleanStageName(name));
    },
    submissionSuccessfulAlert() {
      const id = VSA_SETTINGS_FORM_SUBMISSION_SUCCESS_ALERT_ID;
      const message = sprintf(
        this.isEditing
          ? this.$options.i18n.SETTINGS_FORM_UPDATED
          : this.$options.i18n.SETTINGS_FORM_CREATED,
        { name: this.name },
      );

      return { id, message, variant: 'success' };
    },
  },
  watch: {
    createValueStreamErrors: 'refreshErrors',
  },
  created() {
    this.refreshErrors();
  },
  methods: {
    ...mapActions(['createValueStream', 'updateValueStream']),
    async onSubmit() {
      this.showSubmitError = false;
      this.validate();
      if (this.hasFormErrors) return;

      let req = this.createValueStream;
      let params = {
        name: this.name,
        stages: formatStageDataForSubmission(this.stages, this.isEditing),
      };
      if (this.isEditing) {
        req = this.updateValueStream;
        params = {
          ...params,
          id: this.initialData.id,
        };
      }

      this.isSubmitting = true;

      const response = await req(params);

      if (this.hasFormErrors) {
        this.isSubmitting = false;
        this.showSubmitError = true;
        return;
      }

      this.track('submit_form', {
        label: this.isEditing ? 'edit_value_stream' : 'create_value_stream',
      });

      const redirectPath = mergeUrlParams({ value_stream_id: response.data.id }, this.vsaPath);
      visitUrlWithAlerts(redirectPath, [this.submissionSuccessfulAlert]);
    },
    stageGroupLabel(index) {
      return sprintf(this.$options.i18n.STAGE_INDEX, { index: index + 1 });
    },
    recoverStageTitle(name) {
      return sprintf(this.$options.i18n.HIDDEN_DEFAULT_STAGE, { name });
    },
    hasDirtyName(current, original) {
      return current.trim().toLowerCase() !== original.trim().toLowerCase();
    },
    validateStages() {
      return this.stages.map((stage) =>
        validateStage({
          currentStage: stage,
          allStageNames: this.currentValueStreamStageNames,
          labelEvents: getLabelEventsIdentifiers(this.formEvents),
        }),
      );
    },
    refreshErrors() {
      const { defaultStageConfig, selectedPreset, createValueStreamErrors = {} } = this;
      const { name = [], stages = [{}] } = createValueStreamErrors;
      this.nameErrors = name;
      this.stageErrors =
        cloneDeep(stages) || initializeStageErrors(defaultStageConfig, selectedPreset);
    },
    validate() {
      const { name } = this;
      this.nameErrors = validateValueStreamName({ name });
      this.stageErrors = this.validateStages();
    },
    moveItem(arr, index, direction) {
      return direction === STAGE_SORT_DIRECTION.UP
        ? swapArrayItems(arr, index - 1, index)
        : swapArrayItems(arr, index, index + 1);
    },
    handleMove({ index, direction }) {
      const newStages = this.moveItem(this.stages, index, direction);
      const newErrors = this.moveItem(this.stageErrors, index, direction);
      this.stages = cloneDeep(newStages);
      this.stageErrors = cloneDeep(newErrors);
    },
    validateStageFields(index) {
      const copy = [...this.stageErrors];
      copy[index] = validateStage({ currentStage: this.stages[index] });
      this.stageErrors = copy;
    },
    fieldErrors(index) {
      return this.stageErrors && this.stageErrors[index] ? this.stageErrors[index] : {};
    },
    onHide(index) {
      const target = this.stages[index];
      this.stages = [...this.stages.filter((_, i) => i !== index)];
      this.hiddenStages = [...this.hiddenStages, target];
    },
    onRemove(index) {
      const newErrors = this.stageErrors.filter((_, idx) => idx !== index);
      const newStages = this.stages.filter((_, idx) => idx !== index);
      this.stages = [...newStages];
      this.stageErrors = [...newErrors];
    },
    onRestore(hiddenStageIndex) {
      const target = this.hiddenStages[hiddenStageIndex];
      this.hiddenStages = [...this.hiddenStages.filter((_, i) => i !== hiddenStageIndex)];
      this.stages = [
        ...this.stages,
        { ...target, transitionKey: uniqueId(`stage-${target.name}-`) },
      ];
    },
    lastStage() {
      const stages = this.$refs.formStages;
      return stages[stages.length - 1];
    },
    async scrollToLastStage() {
      await this.$nextTick();
      // Scroll to the new stage we have added
      this.lastStage().focus();
      this.lastStage().scrollIntoView({ behavior: 'smooth' });
    },
    addNewStage() {
      // validate previous stages only and add a new stage
      this.validate();
      this.stages = [
        ...this.stages,
        { ...defaultCustomStageFields, transitionKey: uniqueId('stage-') },
      ];
      this.stageErrors = [...this.stageErrors, {}];
    },
    onAddStage() {
      this.addNewStage();
      this.scrollToLastStage();
    },
    onFieldInput(activeStageIndex, { field, value }) {
      const updatedStage = { ...this.stages[activeStageIndex], [field]: value };
      const copy = [...this.stages];
      copy[activeStageIndex] = updatedStage;
      this.stages = copy;
    },
    resetAllFieldsToDefault() {
      this.stages = initializeStages(this.defaultStageConfig, this.selectedPreset);
      this.stageErrors = initializeStageErrors(this.defaultStageConfig, this.selectedPreset);
    },
    handleResetDefaults() {
      if (this.isEditing) {
        const {
          initialData: { name: initialName, stages: initialStages },
        } = this;
        this.name = initialName;
        this.nameErrors = [];
        this.stages = initializeStages(initialStages);
        this.stageErrors = [{}];
      } else {
        this.resetAllFieldsToDefault();
      }
    },
    onSelectPreset() {
      if (this.selectedPreset === PRESET_OPTIONS_DEFAULT) {
        this.handleResetDefaults();
      } else {
        this.resetAllFieldsToDefault();
      }
    },
    restoreActionTestId(index) {
      return `stage-action-restore-${index}`;
    },
  },
  i18n,
};
</script>
<template>
  <div>
    <gl-alert
      v-if="showSubmitError"
      variant="danger"
      class="gl-mb-3"
      @dismiss="showSubmitError = false"
    >
      {{ $options.i18n.SUBMIT_FAILED }}
    </gl-alert>
    <gl-form>
      <gl-form-group
        data-testid="create-value-stream-name"
        label-for="create-value-stream-name"
        :label="$options.i18n.FORM_FIELD_NAME_LABEL"
        :invalid-feedback="invalidNameFeedback"
        :state="isValueStreamNameValid"
      >
        <div class="gl-flex gl-justify-between">
          <gl-form-input
            id="create-value-stream-name"
            v-model.trim="name"
            name="create-value-stream-name"
            data-testid="create-value-stream-name-input"
            :placeholder="$options.i18n.FORM_FIELD_NAME_PLACEHOLDER"
            :state="isValueStreamNameValid"
            required
          />
          <transition name="fade">
            <gl-button
              v-if="canRestore"
              data-testid="vsa-reset-button"
              class="gl-ml-3"
              variant="link"
              @click="handleResetDefaults"
              >{{ $options.i18n.RESTORE_DEFAULTS }}</gl-button
            >
          </transition>
        </div>
      </gl-form-group>
      <gl-form-radio-group
        v-if="!isEditing"
        v-model="selectedPreset"
        class="gl-mb-4"
        data-testid="vsa-preset-selector"
        :options="presetOptions"
        name="preset"
        @input="onSelectPreset"
      />
      <div data-testid="extended-form-fields">
        <transition-group name="stage-list" tag="div">
          <div
            v-for="(stage, activeStageIndex) in stages"
            ref="formStages"
            :key="stage.id || stage.transitionKey"
          >
            <hr class="gl-my-5" />
            <custom-stage-fields
              v-if="stage.custom"
              :stage-label="stageGroupLabel(activeStageIndex)"
              :stage="stage"
              :stage-events="formEvents"
              :index="activeStageIndex"
              :total-stages="stages.length"
              :errors="fieldErrors(activeStageIndex)"
              :default-group-labels="defaultGroupLabels"
              @move="handleMove"
              @remove="onRemove"
              @input="onFieldInput(activeStageIndex, $event)"
            />
            <default-stage-fields
              v-else
              :stage-label="stageGroupLabel(activeStageIndex)"
              :stage="stage"
              :stage-events="formEvents"
              :index="activeStageIndex"
              :total-stages="stages.length"
              :errors="fieldErrors(activeStageIndex)"
              @move="handleMove"
              @hide="onHide"
              @input="validateStageFields(activeStageIndex)"
            />
          </div>
        </transition-group>
        <div>
          <div v-if="hiddenStages.length">
            <hr class="gl-mb-2 gl-mt-5" />
            <gl-form-group
              v-for="(stage, hiddenStageIndex) in hiddenStages"
              :key="stage.id"
              data-testid="vsa-hidden-stage"
            >
              <span class="gl-m-0 gl-mr-3 gl-align-middle gl-font-bold">{{
                recoverStageTitle(stage.name)
              }}</span>
              <gl-button
                variant="link"
                :data-testid="restoreActionTestId(hiddenStageIndex)"
                @click="onRestore(hiddenStageIndex)"
                >{{ $options.i18n.RESTORE_HIDDEN_STAGE }}</gl-button
              >
            </gl-form-group>
          </div>
          <hr class="gl-mb-5 gl-mt-2" />
          <value-stream-form-content-actions
            :is-editing="isEditing"
            :is-loading="isSubmitting"
            :value-stream-id="selectedValueStreamId"
            @clickPrimaryAction="onSubmit"
            @clickAddStageAction="onAddStage"
          />
        </div>
      </div>
    </gl-form>
  </div>
</template>
